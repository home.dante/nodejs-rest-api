const express = require("express");
const router = express.Router();

const multer = require("multer");
const checkAuth = require("../middleware/check-auth");
const authorize = require("../middleware/authorize2.0");
const ProductsController = require("../controllers/products");
// const upload = multer({ dest: "uploads/" });
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "_" + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5 // 5 meg
  },
  fileFilter: fileFilter
});

//GET ALL PRODUCTS
router.get("/", ProductsController.products_get_all);

//CREATE PRODUCT
router.put(
  "/",
  checkAuth,
  authorize,
  upload.single("productImage"),
  ProductsController.products_create
);

//GET ONE PRODUCT BY ID
router.get("/:ID", ProductsController.products_get_one_by_id);

//UPDATE PRODUCT BY ID
router.patch("/:ID", checkAuth, authorize, ProductsController.products_update);

//DELETE PRODUCT
router.delete("/:ID", checkAuth, authorize, ProductsController.products_delete);

module.exports = router;
