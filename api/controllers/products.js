const Product = require("../models/product");
const mongoose = require("mongoose");
const serverUrl = process.env.SERVER_URL + "products/";
const User = require("../models/user");

exports.products_get_all = (req, res, next) => {
  Product.find()
    .select("-__v")
    .populate("vendor")
    .exec()
    .then((docs) => {
      const response = {
        request: {
          type: "GET",
          description: "Get all Products",
          url: serverUrl
        },
        count: docs.length,
        products: docs
      };
      console.log(docs);
      if (docs.length > 0) {
        res.status(200).json(response);
      } else {
        res.status(200).json({ message: "No entries found" });
      }
    })
    .catch((err) => {
      console.log(err);
      res.send(500).json({ error: err });
    });
};

exports.products_create = (req, res, next) => {
  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
    price: req.body.price,
    vendor: req.userID,
    productImage: req.file.path,
    owner: req.userID
  });

  let updateUserPromise = User.findOneAndUpdate(
    { _id: product.vendor },
    { $push: { products: product._id } }
  );

  let createProductPromise = product.save();

  Promise.all([updateUserPromise, createProductPromise])
    .then((result) => {
      console.log(result);
      res.status(201).json({
        message: "Created Product successfully",
        createdProduct: {
          name: result[1].name,
          price: result[1].price,
          _id: result[1]._id
        },
        request: {
          type: "PUT",
          url: serverUrl + result[1]._id,
          protected: true
        }
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};
exports.products_get_one_by_id = (req, res, next) => {
  const id = req.params.ID;
  Product.findById(id)
    .populate("vendor", "_id email")
    .exec()
    .then((result) => {
      // console.log(result);
      if (result) {
        res.status(200).json({
          name: result.name,
          price: result.price,
          _id: result._id,
          vendor: result.vendor,
          productImage: result.productImage,
          owner: result.owner,
          request: {
            type: "GET",
            url: serverUrl + result._id
          }
        });
      } else {
        res.status(404).json({ message: "No valid Entry for provided ID" });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};
exports.products_update = (req, res, next) => {
  const id = req.params.ID;
  if (req.body.length) {
    const updateOps = {};
    for (const ops of req.body) {
      if (ops.propName != "owner") updateOps[ops.propName] = ops.value;
      else console.log("User role is not a modifiable field");
    }
    Product.update({ _id: id }, { $set: updateOps })
      .exec()
      .then((result) => {
        console.log(result);
        res.status(200).json({
          message: "Product updated",
          request: {
            type: "PATCH",
            url: serverUrl + id
          }
        });
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  } else {
    res.status(500).json({
      message: "Endpoint accepts only arrays as body parameters",
      request: {
        type: "PATCH",
        url: serverUrl + id,
        description:
          "Updates product by ID. Accepts product ID as URL param, and an array as parameters you wish to modify",
        bodyParams: [
          { propName: "your_property_name" },
          { value: "your_value" }
        ],
        viableProperties: "name:String, price:number"
      }
    });
  }
};
exports.products_delete = (req, res, next) => {
  const id = req.params.ID;
  Product.deleteOne({ _id: id })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json({
        message: "Product deleted",
        request: {
          type: "DELETE"
        }
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
        request: {
          type: "DELETE",
          params: [{ ID: "Number" }]
        }
      });
    });
};
