const Order = require("../models/order");
const serverUrl = process.env.SERVER_URL + "orders/";
const Product = require("../models/product");
const mongoose = require("mongoose");
const _ = require("lodash");

//GET ALL ORDERS
exports.orders_get_all = (req, res, next) => {
  Order.find({})
    .populate("products.product", "-__v")
    .populate("user", "-__v")
    .select("-__v")
    .exec()
    .then((result) => {
      console.log(result);
      // res.status(200).json({ orders: result });
      res.status(200).json({
        request: {
          type: "GET",
          url: serverUrl,
          protected: true
        },
        count: result.length,
        orders: result
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

//DELETE ORDERS
exports.orders_delete_by_id = (req, res, next) => {
  const id = req.params.orderID;
  Order.deleteOne({ _id: id })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json({
        message: "Order deleted",
        request: {
          type: "DELETE",
          protected: true,
          url: serverUrl + id
        }
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};
//GET ONE ORDER BY ID
exports.orders_get_one_by_id = (req, res, next) => {
  const id = req.params.ID;
  Order.findById(id)
    .populate("productID", "-__v")
    .select("-__v")
    .exec()
    .then((result) => {
      if (!result) {
        return res
          .status(404)
          .json({ message: "No valid Entry for provided ID" });
      }

      res.status(200).json({
        order: result,
        request: {
          type: "GET",
          url: serverUrl + result._id,
          protected: true
        }
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

//CREATE ORDER
exports.orders_create = (req, res, next) => {
  const cart = req.body.cart;
  let ids = [];
  //GETTING PRODUCT ID'S TOGETHER IN AN ARRAY
  cart.forEach((element) => {
    ids.push(element.product);
  });
  //GETING RID OF DUPLICATE ID'S IN AN ARRAY
  const uniqueIds = _.uniq(ids);
  //FETCHING AN ARRAY OF UNIQUE ID'S FROM THE CART
  Product.find({ _id: { $in: ids } })
    // .populate("product")
    .exec()
    .then((result) => {
      console.log(result);
      //RETURNING 500 ERROR IF NUMBER OF ID'S RETURNED FROM THE DATABASE IS LOWER THAN THAT OF SUPPLIED UNIQUE IDS
      if (result.length !== uniqueIds.length) {
        res.status(500).json({ message: "One or more products do not exist" });
      }
      //CALCULATING TOTALS
      let total = 0;
      cart.forEach((item) => {
        //FOR EACH ITEM IN CART CHECK BY ID IN ARRAY RESULTED FROM THE DATABASE WHAT THE PRICE IS
        let resultItem = result.find(function(element) {
          if (element._id == item.product) return element;
          else return false;
        });
        if (resultItem) total += resultItem.price * item.quantity;
        //RETURN ERROR IF CAN'T FIND PRODUCT ID AND FETCH  ITS PRICE
        else
          res.status(500).json({
            error: "Unable to fetch product ID for" + resultItem.name
          });
      });
      //ATTEMPTING TO CREATE NEW ORDER

      const order = new Order({
        _id: new mongoose.Types.ObjectId(),
        products: cart,
        total: total,
        user: req.userID, //*
        owner: req.userID
      });
      //*note: req.userID is set in the middleware, by extracting it from the jwt token, then passed into req like this - req.userID= decodedToken.userID
      //SAVE AND CHAIN NEXT PROMISE
      return order.save();
    })
    .then((order) => {
      //RETURNING CREATED ORDER
      res.status(200).json({
        result: order,
        request: {
          type: "PUT",
          url: serverUrl + order._id,
          protected: true
        }
      });
    })
    .catch((err) => {
      console.log("CREATE");
      console.log(err);
      res.status(500).json({ error: err });
    });
};
//END OF CREATE ORDER
