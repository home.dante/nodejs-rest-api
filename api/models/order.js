const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  products: [
    {
      product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
        required: true
      },
      quantity: { type: Number, default: 1 }
    }
  ],
  total: {
    type: Number
  },

  user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
  owner: { type: mongoose.Schema.Types.ObjectId, required: true }
});
orderSchema.pre("save", function(next) {
  orderSchema.owner = orderSchema._id;

  this.total = 150;
  next();
});

module.exports = mongoose.model("Order", orderSchema);
