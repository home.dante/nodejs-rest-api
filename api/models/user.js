const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  email: {
    type: String,
    required: true,
    unique: true,
    dropDups: true,
    match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  },
  password: { type: String, required: true },
  role: { type: String, required: true, default: "customer" },
  orders: [{ type: mongoose.Schema.Types.ObjectId, ref: "Order" }],
  products: [{ type: mongoose.Schema.Types.ObjectId, ref: "Product" }],
  owner: { type: mongoose.Schema.Types.ObjectId, required: true }
});
userSchema.index({ email: 1 }, (err, result) => {
  console.log(result);
});

module.exports = mongoose.model("User", userSchema);
