const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, unique: true, required: true, dropDups: true },
  price: { type: Number, required: true },
  productImage: { type: String, required: true },
  vendor: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
  owner: { type: mongoose.Schema.Types.ObjectId, required: true }
});
// productSchema.index({ name: 1 }, (err, result) => {
//   console.log(result);
//   // cb(result);
// });
module.exports = mongoose.model("Product", productSchema);
