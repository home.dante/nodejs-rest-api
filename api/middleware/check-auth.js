const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    // console.log(token);
    const decoded = jwt.verify(token, process.env.JWT_KEY);
    // console.log("decoded token: \n", decoded, "\n");
    req.userData = decoded;
    req.userID = decoded.userID;
    console.log("user id is : ", decoded.userID);
    // if (decoded.role === "user") {
    // }
    next();
  } catch (error) {
    return res.status(401).json({ message: "Auth failed" });
  }
};
