const http = require("http");
const app = require("./app");
const mongoose = require("mongoose");
const config = require("./config/database");
const Product = require("./api/models/product");

// mongoose.set('createIndexes', true);
// mongoose.collection.createIndex()

//Connect to database
mongoose
  .connect(
    config.database,
    { useNewUrlParser: true }
  )
  .then((res) => {
    console.log("Connection to the database: \n");
    console.log("<<< " + config.database + " >>> \n");
  })
  .catch((err) => {
    console.log(err);
  });

//On Connection

mongoose.connection.on("connected", () => {
  console.log("connected to database " + config.database);
});

//On Error
mongoose.connection.on("error", (err) => {
  console.log("database error" + err);
});

const port = process.env.PORT || 3000;

const server = http.createServer(app);

server.listen(port);
