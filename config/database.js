module.exports = {
  // database: 'mongodb://home.dante:447411abcd@ds137862.mlab.com:37862/mean-auth-local',
  //   database: "mongodb://REST-app:447411@" + process.env.MONGO_DB_URL
  database: `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PW}@${
    process.env.MONGO_DB_URL
  }`
};
// db.createUser({
//     "user": "REST-app",
//     "pwd": "447411",
//     "roles": [
//       { "role": "readWrite", "db": "nodejs-tutorial" },
//       { "role": "read", "db": "reporting" }
//     ]
//   })
